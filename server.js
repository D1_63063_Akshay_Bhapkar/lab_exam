const { response } = require('express')
const express = require('express')
const app = express()

app.get('/',(request,response)=>{
    response.send("Welcome Here")
})

app.listen(4000,'0.0.0.0',()=>{
    console.log('server started at port 4000')
    process.exit()
})